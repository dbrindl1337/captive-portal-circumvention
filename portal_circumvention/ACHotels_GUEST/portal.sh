base_url="https://login.mikenopa.com/"
action="/de_DE/action/"
const_connect_device_task="connect-device-task"
const_pms_login="pms-login"
const_start_pms_authentication_task="start-pms-authentication-task"
const_select_plan="select-plan"
const_activate_connection_option_task="activate-connection-option-task"

#echo "Please put in your roon name"
#read user_name
user_name=$(cat credentials.secret | head -1)
echo "Please put in your room number"
read user_room

echo "initiating variables"

url=$(curl -s 'http://www.msftconnecttest.com/' | grep -oP "URL=\K.*" | head -c-3)
echo "the url is $url"
login_url=$(curl -s $url -c cookie | grep "LoginURL" | grep "LoginURL" | head -c-12 | cut -c13-)
echo "the login url is $login_url"
idstring=$(echo $login_url | grep -oP "login/\K.*")
echo "the idstring is $idstring"

echo "initiating negotiation"
# connect-device-task
echo "invoking connect device task"
curl -s -L ${base_url}${idstring}${action}${const_connect_device_task} -b cookie > /dev/null
# pms-login with account data
echo "transferring account data"
curl -s -L ${base_url}${idstring}${action}${const_pms_login} --data 'roomNumber='$user_room'&surname='$user_name'&glf_send_marriottRememberThisDevice=send&send=Verbinden&proceed=fastresident' -b cookie > /dev/null
# sleep to give the auth task time
echo "sleeping for 5 seconds"
sleep 5
# start-pms-authentication-task with other referrer, dunno why this is necessary
echo "invoking pms_auth_task"
curl -s -L ${base_url}${idstring}${action}${const_start_pms_authentication_task} -H 'Referer: '${base_url}${idstring}${action}${const_start_pms_authentication_task} -b cookie > /dev/null
### This is for the free option
# save plan id, take the first one for a day, or the last one with (| tail -1) for 1 week
#plan_id=$(curl -s ${base_url}${idstring}${action}${const_select_plan} -H 'Referer: '${base_url}${idstring}${action}${const_start_pms_authentication_task} -b cookie | grep 'data-is-free=\"1\"' | grep -oP "\s+value=\"\K.[^ \"]*" -m 1)
#echo "plan id is: $plan_id"
# select-plan
echo "selecting plan"
#curl -s -L ${base_url}${idstring}${action}${const_select_plan} -H 'Referer: '${base_url}${idstring}${action}${const_select_plan} --data 'selected_plan='"${plan_id}"'&buy=Jetzt+Verbinden' -b cookie > /dev/null
curl -s -L ${base_url}${idstring}${action}${const_select_plan} -H 'Referer: '${base_url}${idstring}${action}${const_select_plan} -b cookie --data 'do_connect=Verbinden' > /dev/null
# activate connection option
echo "activated!"
curl -s -L ${base_url}${idstring}${action}${const_activate_connection_option_task} -H 'Referer: '${base_url}${idstring}${action}${const_select_plan} -b cookie > /dev/null

rm cookie