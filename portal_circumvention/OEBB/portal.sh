echo "getting token ..."
response=$(curl -L msftconnecttest.com --cookie-jar cookie)
token=$(echo "$response" | grep "_token" | grep -o "value=\".*\"" | head -c-2 | cut -c8-)
echo "token: "
echo "$token"
curl 'https://railnet.oebb.at/connecttoweb' --data '_token='"$token"'&_ceid=35&checkit=on&form_type=registration' -b cookie
rm cookie