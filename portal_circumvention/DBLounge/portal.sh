response=$(curl -s 'https://public-wifi.deutschebahn.com/portal_api.php' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Referer: https://public-wifi.deutschebahn.com/145/portal/' -H 'Origin: null' -H 'X-Requested-With: XMLHttpRequest' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3740.0 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' --data 'action=subscribe&type=one&connect_policy_accept=false&user_login=&user_password=&user_password_confirm=&email_address=&prefix=&phone=&policy_accept=false&gender=&interests=' --compressed)

login=$(echo $response | jq '.info.subscribe.login')
password=$(echo $response | jq '.info.subscribe.password')

login=$(sed -e 's/^"//' -e 's/"$//' <<< "$login")
password=$(sed -e 's/^"//' -e 's/"$//' <<< "$password")

curl -s 'https://public-wifi.deutschebahn.com/portal_api.php' -H 'Origin: https://public-wifi.deutschebahn.com' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3740.0 Safari/537.36' -H 'Content-Type: application/x-www-form-urlencoded' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'Referer: https://public-wifi.deutschebahn.com/145/portal/' -H 'X-Requested-With: XMLHttpRequest' -H 'Connection: keep-alive' --data 'action=authenticate&login='"$login"'&password='"$password"'&policy_accept=false&from_ajax=true&wispr_mode=false' --compressed > /dev/null