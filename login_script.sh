chosenSSID="WIFI"
chosenBSSID="00:00:00:00:00:00"
config_folder="saved_logins"
portal_folder="portal_circumvention"

scan(){
#scan for wifis
echo "scanning for wifis...";
echo "these are the available ones:";
curl 'http://192.168.0.1/cgi?7' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[ACT_WLAN_SCAN#1,1,0,0,0,0#0,0,0,0,0,0]0,0\r\n' > /dev/null;
if [ "$1" == "withBSSID" ]; then
	curl 'http://192.168.0.1/cgi?6' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN_BSSDESC_ENTRY#0,0,0,0,0,0#1,1,0,0,0,0]0,7\r\nSSID\r\nBSSID\r\nSecurityEnable\r\nChannel\r\nRSSI\r\nauthMode\r\nwepStatus\r\n' | grep ".*SSID" | cut -c 6-;
else
	curl 'http://192.168.0.1/cgi?6' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN_BSSDESC_ENTRY#0,0,0,0,0,0#1,1,0,0,0,0]0,7\r\nSSID\r\nBSSID\r\nSecurityEnable\r\nChannel\r\nRSSI\r\nauthMode\r\nwepStatus\r\n' | grep "^SSID" | sort -u | cut -c 6-;
fi	
}

#finds every prefix for now, not exact match
get_bssid(){
	curl 'http://192.168.0.1/cgi?7' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[ACT_WLAN_SCAN#1,1,0,0,0,0#0,0,0,0,0,0]0,0\r\n' > /dev/null;
	chosenBSSID=$(curl 'http://192.168.0.1/cgi?6' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN_BSSDESC_ENTRY#0,0,0,0,0,0#1,1,0,0,0,0]0,7\r\nSSID\r\nBSSID\r\nSecurityEnable\r\nChannel\r\nRSSI\r\nauthMode\r\nwepStatus\r\n' | grep "$1" -A1 -m1 | grep "BSSID" | cut -c 7-)
}

usage(){
cat help.txt
}

circumvent_portal(){

connected=$(inet_connected 1)

echo "You are connected to SSID: $1"

if [ "$connected" == 0 ]; then 
	echo "you are already connected to the internet, no need to circumvent portal"
	return 0
else
	echo "Internet connection check failed, trying to recognize captive portal provider ..."
fi

if [ ! -d $portal_folder/$1 ]; then
		echo "No circumvention script found ..."
		return 1
fi

echo "$1"

echo "config found, circumventing portal ..."

cd $portal_folder/$1/

./portal.sh

cd ../../

}

testing(){
echo $1
curl 'http://192.168.0.1/cgi?2&2&2&2' -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN#1,1,0,0,0,0#0,0,0,0,0,0]0,10\r\nSSID=SK\r\nStandard=n\r\nAutoChannelEnable=0\r\nChannel=6\r\nX_TP_Bandwidth=Auto\r\nEnable=1\r\nSSIDAdvertisementEnabled=1\r\nWMMEnable=1\r\nX_TP_FragmentThreshold=2346\r\nRegulatoryDomain=DE \r\n[LAN_WLAN_MULTISSID#1,1,0,0,0,0#0,0,0,0,0,0]1,1\r\nmultiSSIDEnable=0\r\n[LAN_WLAN_WDSBRIDGE#1,2,0,0,0,0#0,0,0,0,0,0]2,1\r\nBridgeEnable=0\r\n[LAN_WLAN_WDSBRIDGE#1,1,0,0,0,0#0,0,0,0,0,0]3,7\r\nBridgeEnable=1\r\nBridgeBSSID='"$2"$'\r\nBridgeSSID='"$1"$'\r\nBridgeAuthMode=Open\r\nBridgeEncryptMode=None\r\nBridgeKey=\r\nBridgeWepKeyIndex=1\r\n'
}

login(){
	echo "trying to transmit data to router"
	curl 'http://192.168.0.1/cgi?2&2&2&2' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN#1,1,0,0,0,0#0,0,0,0,0,0]0,10\r\nSSID=SK\r\nStandard=n\r\nAutoChannelEnable=0\r\nChannel=6\r\nX_TP_Bandwidth=Auto\r\nEnable=1\r\nSSIDAdvertisementEnabled=1\r\nWMMEnable=1\r\nX_TP_FragmentThreshold=2346\r\nRegulatoryDomain=DE \r\n[LAN_WLAN_MULTISSID#1,1,0,0,0,0#0,0,0,0,0,0]1,1\r\nmultiSSIDEnable=0\r\n[LAN_WLAN_WDSBRIDGE#1,2,0,0,0,0#0,0,0,0,0,0]2,1\r\nBridgeEnable=0\r\n[LAN_WLAN_WDSBRIDGE#1,1,0,0,0,0#0,0,0,0,0,0]3,7\r\nBridgeEnable=1\r\nBridgeBSSID='"$2"$'\r\nBridgeSSID='"$1"$'\r\nBridgeAuthMode=Open\r\nBridgeEncryptMode=None\r\nBridgeKey=\r\nBridgeWepKeyIndex=1\r\n' > /dev/null
	echo "saving ..."
	curl 'http://192.168.0.1/cgi?5' -s	-H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN#0,0,0,0,0,0#0,0,0,0,0,0]0,18\r\nname\r\nStandard\r\nSSID\r\nRegulatoryDomain\r\nPossibleChannels\r\nRegulatoryDomain\r\nAutoChannelEnable\r\nChannel\r\nX_TP_Bandwidth\r\nEnable\r\nSSIDAdvertisementEnabled\r\nBeaconType\r\nBasicEncryptionModes\r\nWPAEncryptionModes\r\nIEEE11iEncryptionModes\r\nX_TP_Configuration_Modified\r\nWMMEnable\r\nX_TP_FragmentThreshold\r\n' > /dev/null;
	inet_connected 2

}


##TODO Prüft der nur auf Präfixe?
# Telekom/Telekom_FON
auto_login(){

	scan onlySSID
	
	if [ ! -f $config_folder/logins ]; then
		echo "No config file found :("
	fi
	
	scanning_list=$(curl 'http://192.168.0.1/cgi?6' -s -H 'Referer: http://192.168.0.1/mainFrame.htm' --data-binary $'[LAN_WLAN_BSSDESC_ENTRY#0,0,0,0,0,0#1,1,0,0,0,0]0,7\r\nSSID\r\nBSSID\r\nSecurityEnable\r\nChannel\r\nRSSI\r\nauthMode\r\nwepStatus\r\n' | grep -E "B{0,1}SSID" | cut -c 6-)
	
	for SSID in $(cat $config_folder/logins)
		do
			# grep exact match
			grep -q -P '(^|\s)\K'$SSID'(?=\s|$)' <<< "$scanning_list"
			if [ $? == 0 ]; then
				echo "found $SSID in list!"
				chosenSSID=$SSID
				get_bssid "$SSID"
				login "$SSID" "$chosenBSSID"
				return 0
			fi
		done
	

}

save_ssid(){
	if [ ! -d $config_folder ]; then
		mkdir $config_folder
	fi
	if [ ! -f $config_folder/logins ]; then
		touch $config_folder/logins
	fi
	
	grep "$1" $config_folder/logins -q
	
	if [ "$?" != 0 ]; then
		echo "$1" >> $config_folder/logins
		echo "SSID $1 has been saved!"
	fi
}

inet_connected(){
	wget --spider --quiet --timeout $1 https://connectivitycheck.gstatic.com/generate_204
	echo "$?"
}

connectivitiy_test(){
wget --quiet --timeout 15 $1
if [ "$?" == 0 ]; then
  echo "Connected to the router"
else
  echo "Could not reach router"
  # exit 0
fi
}

if [ -z "$1" ]; then
  usage
  exit 0
fi

#connectivitiy_test "192.168.0.1"

while [ "$1" != "" ]; do
    case $1 in
        -s | --scan )           shift
                                scan onlySSID
                                ;;
        --full-scan )           scan withBSSID
                                ;;
        -a | --auto-login )   	auto_login
                                ;;
        -c | --circumvent )   	circumvent_portal $chosenSSID
								;;
        -o | --only-circumvent )shift
								chosenSSID=$1
								circumvent_portal $chosenSSID
                                ;;
        -l | --login )    		shift
								chosenSSID=$1
								get_bssid $1
								if [ "$chosenBSSID" == "" ]; then
									echo "no network with SSID $1 found"
									exit 0
								else
									echo "Found BSSID $chosenBSSID for SSID $1"
									login $1 $chosenBSSID
									save_ssid $chosenSSID
								fi
                                ;;
        -t | --test )    		shift
								testing $1 $chosenBSSID
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done












