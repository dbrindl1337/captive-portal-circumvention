
Usage:
      -h                        Displays this help page. This page will also show when the script is
                                called without parameters

      -s | --scan               Advises the hotspot router to scan for available networks.
                                This option will only show distinct SSIDs.
                                Will print out the list.

      --full-scan               Advises the hotspot router to scan for available networks.
                                This option will show every MAC that sent out a beacon along with
                                it's SSID.
                                Also prints out the list.

      -a | --auto-login         Advises the hotspot router to scan for available networks looks up
                                the found networks in a list of known networks and connects the
                                router to it.

      -c | --circumvent         Connects the router to the specified SSID and executes the circum-
      [SSID]                    vention script

      -o | --only-circumvent    Does not connect to the router, but only execute the circumvention
      [SSID]                    script

      -l | --login              Connects the router to the specified SSID.
      [SSID]

