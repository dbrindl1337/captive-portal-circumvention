## Idea

While travelling, it is common to use wifi hotspots that offer internet access after some confirmation action on a captive portal (splash) page.
I use these kind of hotspots almost every day and figured that there has to be a way to ease the negotiation between my devices and the portal.
After all, if you can get access to the internet via clicking some buttons on a web page in your browser, you can just emulate this behaviour with some
curl action right?
This is how this project started.
An essential part of this automation project is also a compact, usb-powered router, the TP-Link TL-WR902AC v3.
This little device offers an ethernet port, 2.4 and 5 GHz Wifi, and the ability to connect to a wifi network as a client and redistribute the internet connection
from any interface to every other, including the same wifi frequency band.
That means you can connect to a hotspot on 2.4 GHz and offer your own hotspot on the same channel. Devices connected to the router can either be transparent
throughout the gateway (client mode) or disappear behind it (share hotspot mode). The latter comes in handy if only a limited number of devices may be
connected to a hotspot at once.

---

## Tools

I wanted to approach this project by building it up as platform independent as possible and decided to use only basic tools already provided
in a standard shell (or easy to be installed).
This enables it to be run on almost any system, even phones, without going through too much hassle with installing a python runtime or nodejs.
It focusses around standard bash functionality, wget, curl, grep, and maybe a few additions in the future.
I have recently decided to introduce jq, a json parser for bash and will update the older scripts soon.
Reading information out of a json response with standard bash tools is always difficult to read and error-prone, having a tool taking care of that is worth it.

---

## Usage

The repo already provides you with everything you need. Start by getting to know login_script.sh. Most of the functionality configures the router.
([This article](http://pwn2learn.dusuel.fr/blog/unauthenticated-root-shell-on-tp-link-tl-wr902ac-router/) showed me some hacks with which it was
easier to handle the CGI and also supplied me with some information to secure it, many thanks to the author)
If you just want to use the portal circumvention scripts, you could just use it with

-o SSID

while swapping SSID with the SSID of the wifi you are connected to in the moment. The -o switch will advise it to only execute the circumvention script
for the wifi you are currently connected to.

Note that there are some scripts that require a user/password authentication like the Telekom hotspots in Germany. You have to create this file yourself and place it in the script's folder.

---

## Background

I have also carried together some knowledge about the way captive portals work for the kind reader.
This section is a work in progress.

Most captive portals simply work by redirecting the http call to any site to their splash page.
This is why many of them don't "work" when you google something. Google restricts non-https connections (even uses hsts) and therefore a call cannot be
redirected to any other page without at least a warning from the browser. Fortunately there are services like [neverssl](neverssl.com), that will, as the name suggests, never use ssl.
Software providers have even created their own standard ways to detect captive portals.

Some of these are


[Google's generate 204](https://connectivitycheck.gstatic.com/generate_204)

[Apple's portal check](captive.apple.com)

[Firefox' detect portal](detectportal.firefox.com)

[Microsoft's connect test](msftconnecttest.com)


Your device will call a service like at upon connecting to a network.
They work by providing a simple response to the call that a client can process. If it gets the expected answer, you are connected to the internet.
If not, something pops up on your screen.
This may be the "Sign in to network" notification an Android, or a browser opening.
After this, the user has to input credentials or accept some terms.

As you could guess, these are mostly workarounds and they are only as good as it gets.
Throughout my fiddling with curl it would happen that my windows machine thought that it was already connected to the internet, because
msftconnecttest.com would provide the correct response, but every other call would be redirected.
Also bear in mind, that these methods are a technically poor way to "force" the user to accept the terms of usage. Usually these networks are
insecure, not only because they don't use WPA2, also the authentication of users is by no means safe.
After you are connected you are simply identified by your MAC address, which means that traffic from your MAC is allowed (for a limited time).
Thus any other client which uses your mac after would be allowed too. Some hotspots provide a log out though.

You see that captive portals are just a pro forma solution. Let's get around them.